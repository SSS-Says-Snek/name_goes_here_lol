## Guidelines for contributing

Lol, just contribute. Make a pull request, I'm pretty lonely right now.

To contribute, submit a pull request at [https://github.com/SSS-Says-Snek/name_goes_here_lol/pulls](https://github.com/SSS-Says-Snek/name_goes_here_lol/pulls).
Major changes such as implementing a new currency in the shop would be reviewed by me,
while small changes like balancing items and buffs will... still be reviewed by me.

That's it for now lol

## Requirements for contributing
- [X] Contributions must solve a problem and/or fix a bug
- [X] Contributors are expected to follow the project's Code of Conduct
- [X] Contributors must not add harmful contributions to the project
