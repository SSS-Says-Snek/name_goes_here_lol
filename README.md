![Github Release Badge](https://img.shields.io/github/v/release/SSS-Says-Snek/name_goes_here_lol?include_prereleases)
![Github Repo Size Badge](https://img.shields.io/github/repo-size/SSS-Says-Snek/name_goes_here_lol)
![Commits since last version](https://img.shields.io/github/commits-since/SSS-Says-Snek/name_goes_here_lol/latest?color=%28255%2C%200%2C%200%29&include_prereleases)

Title's kinda misleading, it's just Snake+ (upgrade version of snake)

Right now, Snake+ should just be an upgraded version of snake, with progress, **ENEMIES**, and fun. Not good at working with classes, so if you look at my code it's gonna look like trash.

Only menu's functioning rn lol

## Todo and Checklists

- [X] Added bare bones for menu
- [ ] Implement actual game mechanics
- [ ] Implement saving and loading system
- [ ] Add actually good graphics
- [ ] Add shopping system for engaging gameplay
- [ ] Add a high score system (preferably switchable between local and overall)
